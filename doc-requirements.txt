-e .
pep517
numpy>=1.14.5
healpy!=1.12.0
gracedb-sdk
matplotlib
astropy
astropy-healpix
scipy>=0.7.2
ligo.skymap>=0.1.1
