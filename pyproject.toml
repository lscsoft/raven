[project]
name = "ligo-raven"
version = "4.0rc0"
authors = [
    {name = "Alex Urban", email = "alexander.urban@ligo.org"},
    {name = "Min-A Cho", email = "min-a.cho@ligo.org"},
    {name = "Brandon Piotrzkowski", email = "brandon.piotrzkowski@ligo.org"}
]
license = {text = "GNU General Public License Version 3"}
description = "Low-latency coincidence search between external triggers and GW candidates"
readme = "README.md"
classifiers = [
    "Intended Audience :: Science/Research",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "Operating System :: OS Independent",
    "Topic :: Internet",
    "Topic :: Scientific/Engineering :: Astronomy",
    "Topic :: Scientific/Engineering :: Physics"
]
requires-python = ">=3.10"
dependencies = [
    "numpy>=1.14.5",
    "healpy!=1.12.0",  # FIXME: https://github.com/healpy/healpy/pull/457
    "gracedb-sdk",
    "hpmoc >= 1.0.0",
    "ligo-gracedb>=2.2.0",
    "matplotlib",
    "astropy",
    "astropy-healpix",
    "scipy>=0.7.2",
    "ligo.skymap>=0.1.1",
    "validators"
]

[tool.setuptools]
script-files = [
    "bin/raven_query",
    "bin/raven_search",
    "bin/raven_skymap_overlap",
    "bin/raven_coinc_far",
    "bin/raven_calc_signif_gracedb",
    "bin/raven_offline_search"
]

[project.optional-dependencies]
test = [
    "pytest",
    "pytest-cov"
]
docs = [
    "pep517",  # https://github.com/matplotlib/matplotlib/pull/28289
    "sphinx >= 4.0",
    "sphinx-argparse >= 0.3.0",  # https://github.com/alex-rudakov/sphinx-argparse/pull/126
    "sphinxcontrib-mermaid >= 0.7.1",  # https://github.com/mgaitan/sphinxcontrib-mermaid/issues/72
    "tomli >= 1.1.0 ; python_version < \"3.11\"",
]

[project.urls]
"Homepage" = "https://git.ligo.org/lscsoft/raven"
"Documentation" = "https://ligo-raven.readthedocs.io"
"Source Code" = "https://git.ligo.org/lscsoft/raven"
"Bug Tracker" = "https://git.ligo.org/lscsoft/raven/issues"

[tool.setuptools.packages.find]
include = ["ligo.*"]

[tool.pytest.ini_options]
testpaths = "ligo/raven/tests"

[tool.coverage.run]
source = ["ligo/raven"]
omit = [
    "*/raven/tests/*"
]
