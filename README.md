Welcome to RAVEN, the Rapid, on-source VOEvent Coincidence Monitor.

* [Quick Start Guide](https://ligo-raven.readthedocs.io/en/latest/quickstart.html)
* [Latest Development Documentation](https://ligo-raven.readthedocs.io/en/latest/)
* [Latest Release Documentation](https://lscsoft.docs.ligo.org/raven)
 
RAVEN is used in the LVK low-latency alert system in order to find associations
between gravitational waves and other astronomical transients via GraceDb. See the
following for more details:
* [gwcelery.tasks.external_triggers.py](https://igwn.readthedocs.io/projects/gwcelery/en/latest/gwcelery.tasks.external_triggers.html)
* [gwcelery.tasks.raven.py](https://igwn.readthedocs.io/projects/gwcelery/en/latest/gwcelery.tasks.raven.html)
* [gwcelery.tasks.external_skymaps.py](https://igwn.readthedocs.io/projects/gwcelery/en/latest/gwcelery.tasks.external_skymaps.html)

RAVEN is also designed to be able to perform these tasks via GraceDb in offline searches
as well.

The latest review documentation can be found at the following:
* https://wiki.ligo.org/Bursts/RavenReviewO4
* https://wiki.ligo.org/Bursts/RavenReview
* https://git.ligo.org/brandon.piotrzkowski/ravenreview

Legacy documentation can be found at the review documentation website:
* https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/GRBreview/RAVEN

